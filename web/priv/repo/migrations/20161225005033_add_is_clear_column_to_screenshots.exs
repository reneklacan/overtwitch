defmodule Overtwitch.Repo.Migrations.AddIsClearColumnToScreenshots do
  use Ecto.Migration

  def change do
    alter table(:screenshots) do
      add :is_clear, :boolean
    end
  end
end
