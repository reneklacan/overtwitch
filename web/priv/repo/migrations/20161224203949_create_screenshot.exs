defmodule Overtwitch.Repo.Migrations.CreateScreenshot do
  use Ecto.Migration

  def change do
    create table(:screenshots) do
      add :path, :string, null: false
      add :checksum, :string, null: false
      add :tag, :string

      timestamps()
    end

    create index(:screenshots, [:checksum])
    create index(:screenshots, [:tag])
  end
end
