defmodule Overtwitch.Repo.Migrations.AddIsImportedColumnToScreenshots do
  use Ecto.Migration

  def change do
    alter table(:screenshots) do
      add :is_imported, :boolean
    end
  end
end
