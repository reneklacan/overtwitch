defmodule Overtwitch.ScreenshotTest do
  use Overtwitch.ModelCase

  alias Overtwitch.Screenshot

  @valid_attrs %{}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Screenshot.changeset(%Screenshot{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Screenshot.changeset(%Screenshot{}, @invalid_attrs)
    refute changeset.valid?
  end
end
