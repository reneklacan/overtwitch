defmodule Twitch do
  def get_request(path, params \\ %{}) do
    params_as_string = params |> Enum.map(fn({k, v}) -> "#{k}=#{v}" end) |> Enum.join("&")
    url = "https://api.twitch.tv/kraken/#{path}?#{params_as_string}"
    client_id = System.get_env("TWITCH_CLIENT_ID")
    headers = [{"Client-Id", client_id}, {"Accept", "application/vnd.twitchtv.v3+json"}]
    HTTPoison.get!(url, headers).body |> Poison.decode!
  end
end

defmodule Twitch.Search.Stream do
  def get(params = %{}) do
    Twitch.get_request("search/streams", params)
  end
end

defmodule Twitch.User do
  def get(nickname) do
    Twitch.get_request("users/#{nickname}")
  end
end
