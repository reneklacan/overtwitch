# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :overtwitch,
  ecto_repos: [Overtwitch.Repo]

# Configures the endpoint
config :overtwitch, Overtwitch.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "0GE/Vm8q6mfmqftKgA35gT0kHSSBLZo0OcTnxgu6nnQxyVA+XCt060yB7+KPRt6G",
  render_errors: [view: Overtwitch.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Overtwitch.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
