defmodule Overtwitch.Router do
  use Overtwitch.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Overtwitch do
    pipe_through :browser # Use the default browser stack

    get "/", HomeController, :index
    post "/fetch", HomeController, :fetch
    post "/import", HomeController, :import
    get "/stats", StatsController, :index
    resources "/votes", VotesController
    get "/tagged", VotesController, :tagged
    post "/export", VotesController, :export
  end

  # Other scopes may use custom stacks.
  # scope "/api", Overtwitch do
  #   pipe_through :api
  # end
end
