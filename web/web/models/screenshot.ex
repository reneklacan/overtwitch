defmodule Overtwitch.Screenshot do
  use Overtwitch.Web, :model

  schema "screenshots" do
    field :path, :string
    field :checksum, :string
    field :tag, :string
    field :is_clear, :boolean
    field :is_imported, :boolean

    timestamps()
  end

  def original_image_path(model) do
    "/media/screenshots/#{model.checksum}.jpg"
  end

  def original_image_url(model) do
    "http://localhost:4000/media/screenshots/#{model.checksum}.jpg"
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:tag, :is_clear])
    |> validate_required([])
  end

  def clear(query) do
    query
    |> where([s], s.is_clear == true)
  end

  def with_hero(query) do
    query
    |> where([s], not s.tag in ["none", "score", "hero_selection", "kill_cam", "menu", "end"])
  end

  def not_tagged(query) do
    query
    |> where([s], is_nil(s.tag))
  end

  def tagged(query) do
    query
    |> where([s], not(is_nil(s.tag)))
  end

  def grouped_by_tag(query) do
    query
    |> select([fragment("tag"), fragment("COUNT(tag)")])
    |> where([s], not(is_nil(s.tag)))
    |> where([s], s.is_clear == true)
    |> where([s], not s.tag in ["none", "score", "hero_selection", "kill_cam", "menu", "end"])
    |> group_by([s], s.tag)
    |> order_by(fragment("COUNT(tag) DESC"))
  end
end
