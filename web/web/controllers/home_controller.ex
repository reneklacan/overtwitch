defmodule Overtwitch.HomeController do
  use Overtwitch.Web, :controller

  def index(conn, _params) do
    conn
    |> render("index.html")
  end

  def fetch(conn, _params) do
    response = Twitch.Search.Stream.get(%{query: "overwatch", limit: 100})

    new_count =
      response["streams"]
      |> Enum.map(fn(stream) -> stream["preview"]["large"] end)
      |> Parallel.pmap(&save_screenshot_from_link/1)
      |> Enum.count(fn x -> x end)

    conn
    |> assign(:new_count, new_count)
    |> put_flash(:info, "Added #{new_count} new screenshots!")
    |> redirect(to: home_path(conn, :index))
  end

  def import(conn, _params) do
    files_to_import = Path.wildcard("import/*.jpg")
    new_count =
      files_to_import
      |> Enum.map(fn filepath ->
          hero = filepath |> String.split(["/", "_"]) |> Enum.at(1)
          save_screenshot_from_filepath(filepath, %{tag: hero, is_imported: true, is_clear: true})
        end)
      |> Enum.count(fn x -> x end)

    files_to_import |> Enum.each(&File.rm!/1)

    conn
    |> put_flash(:info, "Imported #{new_count} new screenshots!")
    |> redirect(to: home_path(conn, :index))
  end

  defp save_screenshot_from_link(link, params \\ %{}) do
    HTTPoison.get!(link).body
    |> save_screenshot(params)
  end

  defp save_screenshot_from_filepath(filepath, params \\ %{}) do
    File.read!(filepath)
    |> save_screenshot(params)
  end

  defp save_screenshot(data, params \\ %{}) do
    checksum = Base.encode16(:erlang.md5(data), case: :lower)

    existing =
      Overtwitch.Screenshot
      |> where([s], s.checksum == ^checksum)
      |> Overtwitch.Repo.one

    if existing == nil do
      filepath = "media/screenshots/#{checksum}.jpg"

      {:ok, file} = File.open(filepath, [:write])
      IO.binwrite(file, data)
      File.close(file)

      default_params = %{checksum: checksum, path: filepath, tag: nil}
      new_screenshot = struct(Overtwitch.Screenshot, Map.merge(default_params, params))
      IO.inspect new_screenshot
      Overtwitch.Repo.insert!(new_screenshot)

      true
    else
      false
    end
  end
end
