defmodule Overtwitch.StatsController do
  use Overtwitch.Web, :controller

  def index(conn, _params) do
    # select tag, count(tag) from screenshots where tag not in ('none') group by tag order by count(tag) desc

    all_count = Overtwitch.Screenshot |> Overtwitch.Repo.aggregate(:count, :id)
    
    tagged_count = Overtwitch.Screenshot |> Overtwitch.Screenshot.tagged |> Overtwitch.Repo.aggregate(:count, :id)

    tagged_clear_count =
      Overtwitch.Screenshot
      |> Overtwitch.Screenshot.tagged
      |> Overtwitch.Screenshot.clear
      |> Overtwitch.Repo.aggregate(:count, :id)

    tagged_hero_count =
      Overtwitch.Screenshot
      |> Overtwitch.Screenshot.tagged
      |> Overtwitch.Screenshot.with_hero
      |> Overtwitch.Repo.aggregate(:count, :id)

    tagged_hero_clear_count =
      Overtwitch.Screenshot
      |> Overtwitch.Screenshot.tagged
      |> Overtwitch.Screenshot.with_hero
      |> Overtwitch.Screenshot.clear
      |> Overtwitch.Repo.aggregate(:count, :id)

    stats =
      Overtwitch.Screenshot
      |> Overtwitch.Screenshot.grouped_by_tag
      |> Overtwitch.Repo.all

    conn
    |> assign(:stats, stats)
    |> assign(:all_count, all_count)
    |> assign(:tagged_count, tagged_count)
    |> assign(:tagged_clear_count, tagged_clear_count)
    |> assign(:tagged_hero_count, tagged_hero_count)
    |> assign(:tagged_hero_clear_count, tagged_hero_clear_count)
    |> render("index.html")
  end
end
