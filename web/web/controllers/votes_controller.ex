defmodule Overtwitch.VotesController do
  use Overtwitch.Web, :controller

  @all_heroes [
    "None": "none",
    "Menu": "menu",
    "Hero Selection": "hero_selection",
    "Score Screen": "score",
    "End Screen": "end",
    "Kill Cam": "kill_cam",
    "Ana": "ana",
    "Bastion": "bastion",
    "D.va": "d.va",
    "Genji": "genji",
    "Hanzo": "hanzo",
    "Junkrat": "junkrat",
    "Lucio": "lucio",
    "McCree": "mccree",
    "Mei": "mei",
    "Mercy": "mercy",
    "Pharah": "pharah",
    "Reaper": "reaper",
    "Reinhardt": "reinhardt",
    "Roadhog": "roadhog",
    "Soldier: 76": "soldier",
    "Sombra": "sombra",
    "Symmetra": "symmetra",
    "Torbjorn": "torbjorn",
    "Tracer": "tracer",
    "Widowmaker": "widowmaker",
    "Winston": "winston",
    "Zarya": "zarya",
    "Zenyata": "zenyata",
  ]

  def new(conn, params) do
    base_query =
      Overtwitch.Screenshot
      |> where([s], is_nil(s.tag))

    if params["id"] do
      base_query = base_query |> where([s], s.id == ^params["id"])
    end

    screenshot =
      base_query
      |> order_by(fragment("RANDOM()"))
      |> limit(1)
      |> Overtwitch.Repo.one

    recognize_request = %{link: Overtwitch.Screenshot.original_image_url(screenshot)} |> Poison.encode!
    api_response =
      HTTPoison.post!("http://localhost:5000/", recognize_request, [{"Content-Type", "application/json"}]).body
      |> IO.inspect
      |> Poison.decode!
    probability = api_response["probability"]
    guessed_tag = api_response["hero"]
    default_tag =
      if probability >= 0.5 do
        guessed_tag
      else
        "none"
      end

    IO.inspect api_response

    remaining_count =
      base_query
      |> Overtwitch.Repo.aggregate(:count, :id)

    tagged_count =
      Overtwitch.Screenshot
      |> where([s], not(is_nil(s.tag)))
      |> Overtwitch.Repo.aggregate(:count, :id)

    conn
    |> assign(:all_heroes, @all_heroes)
    |> assign(:guessed_tag, guessed_tag)
    |> assign(:default_tag, default_tag)
    |> assign(:probability, probability)
    |> assign(:remaining_count, remaining_count)
    |> assign(:tagged_count, tagged_count)
    |> assign(:screenshot, screenshot)
    |> render("new.html")
  end

  def update(conn, %{"id" => id, "screenshot" => %{"tag" => tag, "is_clear" => is_clear}}) do
    screenshot = Overtwitch.Repo.get!(Overtwitch.Screenshot, id)
    changeset = Overtwitch.Screenshot.changeset(screenshot, %{"tag" => tag, "is_clear" => is_clear == "yes"})
    Overtwitch.Repo.update!(changeset)

    redirect conn, to: votes_path(conn, :new)
  end

  def tagged(conn, params) do
    screenshots =
      if params["id"] do
        [Overtwitch.Repo.get!(Overtwitch.Screenshot, params["id"])]
      else
        Overtwitch.Screenshot
        |> where([s], not(is_nil(s.tag)))
        |> where([s], s.is_clear == true)
        |> where([s], not s.tag in ["none", "score", "hero_selection", "kill_cam", "menu", "end"])
        |> Overtwitch.Repo.all
      end

    conn
    |> assign(:screenshots, screenshots)
    |> render("tagged.html")
  end

  def export(conn, _params) do
    screenshots =
      Overtwitch.Screenshot
      |> where([s], not(is_nil(s.tag)))
      |> where([s], s.is_clear == true)
      |> where([s], not s.tag in ["none", "score", "hero_selection", "kill_cam", "menu", "end"])
      |> Overtwitch.Repo.all

    json =
      screenshots
      |> Enum.reduce(%{}, fn screenshot, acc ->
          value = screenshot.checksum <> ".jpg"
          Map.merge(acc, %{screenshot.tag => (acc[screenshot.tag] || []) ++ [value]})
        end)
      |> Poison.encode!(pretty: true)

    {:ok, file} = File.open("export.json", [:write])
    IO.binwrite(file, json)
    File.close(file)

    conn
    |> assign(:json, json)
    |> put_flash(:info, "Successfully exported #{Enum.count(screenshots)} tagged screenshots!")
    |> redirect(to: home_path(conn, :index))
  end
end
