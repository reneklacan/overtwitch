import os
import sys
import numpy as np
import cv2

import core

input = core.load_input()
knn = core.train_knn(input)

video_filename = len(sys.argv) > 1 and sys.argv[1] or 'videos/video.mp4'
video = cv2.VideoCapture(video_filename)
fps = int(round(video.get(cv2.CAP_PROP_FPS)))
frame_id = 0
seconds = 0

while True:
    ret, frame = video.read()

    frame_id += 1

    if frame_id % fps == 0:
        seconds += 1

    if frame_id % fps == 0 and seconds % 5 == 0:
        hero, probability = core.predict_hero(knn, frame)

        if probability >= 0.8:
            cv2.imwrite('frames/{}_frame_{}.jpg'.format(hero, frame_id), frame)
            print(frame_id, hero, probability)

    if not ret:
        break

print(seconds)
