import json
import requests
import cv2
import numpy as np
from flask import Flask, jsonify, request

import core

application = Flask(__name__)

input = core.load_input()
knn = core.train_knn(input)

@application.before_request
def log_request_info():
    application.logger.info('%s %s', request.method, request.full_path)

@application.route('/', methods=['POST'])
def recognize():
    print(request.data)
    params = json.loads(request.data)
    link = params['link']
    response = requests.get(link)

    img = np.asarray(bytearray(response.content), dtype='uint8')
    img = cv2.imdecode(img, cv2.IMREAD_UNCHANGED)

    hero, probability = core.predict_hero(knn, img)

    return jsonify({'hero': hero, 'probability': probability})

if __name__ == '__main__':
    application.run(host='0.0.0.0', port=5000, debug=True)
