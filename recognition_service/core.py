import json
import numpy as np
import cv2

HEROES = []

def preprocess(img):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    lower_white = np.array([0, 0, 170])
    upper_white = np.array([255, 60, 255])

    mask = cv2.inRange(hsv, lower_white, upper_white)

    # shape is (360, 640)
    cropped_mask = mask[295:350, 460:630]

    return cropped_mask

def train_knn(input):
    data = []
    labels = []

    for tag, files in input.items():
        for filename in files:
            img = cv2.imread('../web/media/screenshots/' + filename)
            preprocessed_img = preprocess(img)
            data.append(preprocessed_img)
            labels.append(HEROES.index(tag))

    data = np.array(data).reshape(-1, 55*170).astype(np.float32)
    labels = np.array(labels)

    knn = cv2.ml.KNearest_create()
    knn.train(data, cv2.ml.ROW_SAMPLE, labels)

    return knn

def predict_hero(knn, img):
    k = 10

    input_img = np.array([preprocess(img)]).reshape(-1, 55*170).astype(np.float32)
    ret, result, neighbours, dist = knn.findNearest(input_img, k=k)

    hero_label = int(result[0])
    hero = HEROES[hero_label]
    same_neighbour_count = len(list(filter(lambda label: int(label) == hero_label, neighbours[0])))

    probability = float(same_neighbour_count)/k

    return (hero, probability)

def load_input():
    with open('../web/export.json', 'r') as f:
        input = json.load(f)

    global HEROES
    HEROES = list(input.keys())

    return input
